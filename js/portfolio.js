$(function(){
    $("#elastic_grid_demo").elastic_grid({
        'showAllText' : 'All',
        'filterEffect': 'popup', // moveup, scaleup, fallperspective, fly, flip, helix , popup
        'hoverDirection': true,
        'hoverDelay': 0,
        'hoverInverse': false,
        'expandingSpeed': 500,
        'expandingHeight': 500,
        'items' :
        [
            {
                'title'         : 'Folio 1',
                'description'   : 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage.',
                'thumbnail'     : ['images/folio/small/1.jpg', 'images/folio/small/2.jpg', 'images/folio/small/3.jpg', 'images/folio/small/10.jpg', 'images/folio/small/11.jpg'],
                'large'         : ['images/folio/large/1.jpg', 'images/folio/large/2.jpg', 'images/folio/large/3.jpg', 'images/folio/large/10.jpg', 'images/folio/large/11.jpg'],
                'img_title'     : ['jquery elastic grid 1 ', 'jquery elastic grid 2', 'jquery elastic grid 3', 'jquery elastic grid 4', 'jquery elastic grid 5'],
                'button_list'   :
                [
                    { 'title':'Demo', 'url' : 'http://porfolio.bonchen.net/', 'new_window' : true },
                    { 'title':'Download', 'url':'http://porfolio.bonchen.net/', 'new_window' : false}
                ],
                'tags'          : ['Interior']
            },
            {
                'title'         : 'Folio 2',
                'description'   : 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage.',
                'thumbnail'     : ['images/folio/small/4.jpg', 'images/folio/small/5.jpg', 'images/folio/small/6.jpg', 'images/folio/small/7.jpg'],
                'large'         : ['images/folio/large/4.jpg', 'images/folio/large/5.jpg', 'images/folio/large/6.jpg', 'images/folio/large/7.jpg'],
                'img_title'     : ['jquery elastic grid 6 ', 'jquery elastic grid 7 ', 'jquery elastic grid 8', 'jquery elastic grid 9', 'jquery elastic grid 9'],
                'button_list'   :
                [
                    { 'title':'Demo', 'url' : 'http://porfolio.bonchen.net/', 'new_window' : true },
                    { 'title':'Download', 'url':'http://porfolio.bonchen.net/', 'new_window' : true}
                ],
                'tags'          : ['Landscape']
            },
            {
                'title'         : 'Folio 3',
                'description'   : 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage.',
                'thumbnail'     : ['images/folio/small/15.jpg','images/folio/small/8.jpg', 'images/folio/small/9.jpg', 'images/folio/small/10.jpg'],
                'large'         : ['images/folio/large/15.jpg','images/folio/large/8.jpg', 'images/folio/large/9.jpg', 'images/folio/large/10.jpg'],
                'img_title'     : ['jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid'],
                'button_list'   :
                [
                    { 'title':'Demo', 'url' : 'http://porfolio.bonchen.net/', 'new_window' : true },
                    { 'title':'Download', 'url':'http://porfolio.bonchen.net/', 'new_window' : true}
                ],
                'tags'          : ['Interior', 'Landscape']
            },
            {
                'title'         : 'Folio 4',
                'description'   : 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage.',
                'thumbnail'     : ['images/folio/small/12.jpg', 'images/folio/small/13.jpg', 'images/folio/small/14.jpg', 'images/folio/small/15.jpg', 'images/folio/small/16.jpg'],
                'large'         : ['images/folio/large/12.jpg', 'images/folio/large/13.jpg', 'images/folio/large/14.jpg', 'images/folio/large/15.jpg', 'images/folio/large/16.jpg'],
                'img_title'     : ['jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid'],
                'button_list'   :
                [
                    { 'title':'Demo', 'url' : 'http://porfolio.bonchen.net/', 'new_window' : true },
                    { 'title':'Download', 'url':'http://porfolio.bonchen.net/', 'new_window' : true}
                ],
                'tags'          : ['Residential']
            },
            {
                'title'         : 'Folio 5',
                'description'   : 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage.',
                'thumbnail'     : ['images/folio/small/17.jpg', 'images/folio/small/18.jpg', 'images/folio/small/19.jpg', 'images/folio/small/20.jpg'],
                'large'         : ['images/folio/large/17.jpg', 'images/folio/large/18.jpg', 'images/folio/large/19.jpg', 'images/folio/large/20.jpg'],
                'img_title'     : ['jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid'],
                'button_list'   :
                [
                    { 'title':'Demo', 'url' : 'http://porfolio.bonchen.net/', 'new_window' : true },
                    { 'title':'Download', 'url':'http://porfolio.bonchen.net/', 'new_window' : true}
                ],
                'tags'          : ['Landscape']
            },
            {
                'title'         : 'Folio 6',
                'description'   : 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage.',
                'thumbnail'     : ['images/folio/small/13.jpg','images/folio/small/15.jpg', 'images/folio/small/11.jpg', 'images/folio/small/10.jpg'],
                'large'         : ['images/folio/large/13.jpg','images/folio/large/15.jpg', 'images/folio/large/11.jpg', 'images/folio/large/10.jpg'],
                'img_title'     : ['jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid'],
                'button_list'   :
                [
                    { 'title':'Demo', 'url' : 'http://porfolio.bonchen.net/', 'new_window' : true },
                    { 'title':'Download', 'url':'http://porfolio.bonchen.net/', 'new_window' : true}
                ],
                'tags'          : ['Commercial']
            },
            {
                'title'         : 'Folio 7',
                'description'   : 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage.',
                'thumbnail'     : ['images/folio/small/7.jpg','images/folio/small/8.jpg', 'images/folio/small/9.jpg', 'images/folio/small/10.jpg'],
                'large'         : ['images/folio/large/7.jpg','images/folio/large/8.jpg', 'images/folio/large/9.jpg', 'images/folio/large/10.jpg'],
                'img_title'     : ['jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid'],
                'button_list'   :
                [
                    { 'title':'Demo', 'url' : 'http://porfolio.bonchen.net/', 'new_window' : true },
                    { 'title':'Download', 'url':'http://porfolio.bonchen.net/', 'new_window' : true}
                ],
                'tags'          : ['Residential']
            },
            {
                'title'         : 'Folio 8',
                'description'   : 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage.',
                'thumbnail'     : ['images/folio/small/16.jpg', 'images/folio/small/13.jpg', 'images/folio/small/14.jpg', 'images/folio/small/15.jpg', 'images/folio/small/16.jpg'],
                'large'         : ['images/folio/large/16.jpg', 'images/folio/large/13.jpg', 'images/folio/large/14.jpg', 'images/folio/large/15.jpg', 'images/folio/large/16.jpg'],
                'img_title'     : ['jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid'],
                'button_list'   :
                [
                    { 'title':'Demo', 'url' : 'http://porfolio.bonchen.net/', 'new_window' : true },
                    { 'title':'Download', 'url':'http://porfolio.bonchen.net/', 'new_window' : true}
                ],
                'tags'          : ['Commercial']
            },
            {
                'title'         : 'Folio 9',
                'description'   : 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage.',
                'thumbnail'     : ['images/folio/small/18.jpg', 'images/folio/small/18.jpg', 'images/folio/small/19.jpg', 'images/folio/small/20.jpg'],
                'large'         : ['images/folio/large/18.jpg', 'images/folio/large/18.jpg', 'images/folio/large/19.jpg', 'images/folio/large/20.jpg'],
                'img_title'     : ['jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid'],
                'button_list'   :
                [
                    { 'title':'Demo', 'url' : 'http://porfolio.bonchen.net/', 'new_window' : true },
                    { 'title':'Download', 'url':'http://porfolio.bonchen.net/', 'new_window' : true}
                ],
                'tags'          : ['Landscape']
            },
            {
                'title'         : 'Folio 10',
                'description'   : 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage.',
                'thumbnail'     : ['images/folio/small/11.jpg','images/folio/small/15.jpg', 'images/folio/small/11.jpg', 'images/folio/small/10.jpg'],
                'large'         : ['images/folio/large/11.jpg','images/folio/large/15.jpg', 'images/folio/large/11.jpg', 'images/folio/large/10.jpg'],
                'img_title'     : ['jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid'],
                'button_list'   :
                [
                    { 'title':'Demo', 'url' : 'http://porfolio.bonchen.net/', 'new_window' : true },
                    { 'title':'Download', 'url':'http://porfolio.bonchen.net/', 'new_window' : true}
                ],
                'tags'          : ['Residential']
            },
            {
                'title'         : 'Folio 11',
                'description'   : 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage.',
                'thumbnail'     : ['images/folio/small/3.jpg','images/folio/small/15.jpg', 'images/folio/small/11.jpg', 'images/folio/small/10.jpg'],
                'large'         : ['images/folio/large/3.jpg','images/folio/large/15.jpg', 'images/folio/large/11.jpg', 'images/folio/large/10.jpg'],
                'img_title'     : ['jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid'],
                'button_list'   :
                [
                    { 'title':'Demo', 'url' : 'http://porfolio.bonchen.net/', 'new_window' : true },
                    { 'title':'Download', 'url':'http://porfolio.bonchen.net/', 'new_window' : true}
                ],
                'tags'          : ['Commercial']
            },
            {
                'title'         : 'Folio 12',
                'description'   : 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage.',
                'thumbnail'     : ['images/folio/small/5.jpg','images/folio/small/8.jpg', 'images/folio/small/9.jpg', 'images/folio/small/10.jpg'],
                'large'         : ['images/folio/large/5.jpg','images/folio/large/8.jpg', 'images/folio/large/9.jpg', 'images/folio/large/10.jpg'],
                'img_title'     : ['jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid'],
                'button_list'   :
                [
                    { 'title':'Demo', 'url' : 'http://porfolio.bonchen.net/', 'new_window' : true },
                    { 'title':'Download', 'url':'http://porfolio.bonchen.net/', 'new_window' : true}
                ],
                'tags'          : ['Residential', 'Landscape']
            },
            {
                'title'         : 'Folio 13',
                'description'   : 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage.',
                'thumbnail'     : ['images/folio/small/6.jpg', 'images/folio/small/13.jpg', 'images/folio/small/14.jpg', 'images/folio/small/15.jpg', 'images/folio/small/16.jpg'],
                'large'         : ['images/folio/large/6.jpg', 'images/folio/large/13.jpg', 'images/folio/large/14.jpg', 'images/folio/large/15.jpg', 'images/folio/large/16.jpg'],
                'img_title'     : ['jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid'],
                'button_list'   :
                [
                    { 'title':'Demo', 'url' : 'http://porfolio.bonchen.net/', 'new_window' : true },
                    { 'title':'Download', 'url':'http://porfolio.bonchen.net/', 'new_window' : true}
                ],
                'tags'          : ['Commercial']
            },
            {
                'title'         : 'Folio 14',
                'description'   : 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage.',
                'thumbnail'     : ['images/folio/small/8.jpg', 'images/folio/small/18.jpg', 'images/folio/small/19.jpg', 'images/folio/small/20.jpg'],
                'large'         : ['images/folio/large/8.jpg', 'images/folio/large/18.jpg', 'images/folio/large/19.jpg', 'images/folio/large/20.jpg'],
                'img_title'     : ['jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid'],
                'button_list'   :
                [
                    { 'title':'Demo', 'url' : 'http://porfolio.bonchen.net/', 'new_window' : true },
                    { 'title':'Download', 'url':'http://porfolio.bonchen.net/', 'new_window' : true}
                ],
                'tags'          : ['Landscape']
            },
            {
                'title'         : 'Folio 15',
                'description'   : 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage.',
                'thumbnail'     : ['images/folio/small/9.jpg','images/folio/small/15.jpg', 'images/folio/small/11.jpg', 'images/folio/small/10.jpg'],
                'large'         : ['images/folio/large/9.jpg','images/folio/large/15.jpg', 'images/folio/large/11.jpg', 'images/folio/large/10.jpg'],
                'img_title'     : ['jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid', 'jquery elastic grid'],
                'button_list'   :
                [
                    { 'title':'Demo', 'url' : 'http://porfolio.bonchen.net/', 'new_window' : true },
                    { 'title':'Download', 'url':'http://porfolio.bonchen.net/', 'new_window' : true}
                ],
                'tags'          : ['Commercial', 'Landscape']
            }

        ]
    });
});